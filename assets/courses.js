let token = localStorage.getItem("token");
let adminUser = localStorage.getItem("isAdmin")=== "true"
let adminButton = document.querySelector('#adminButton')
let cardFooter;

if(!adminUser){
	adminButton.innerHTML = null
} else {
	adminButton.innerHTML =
	`
		<div class="col-md-2 offset-md-10">
			<a href="./addCourse.html" class="btn btn-block btn-primary">
				Add Course
			</a>
		</div>
	`
}

fetch("http://localhost:3000/api/courses/all",
	{
		method: "GET",
		headers: {
			"Authorization": `Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {

		if(result.length < 1){
			courseData = `No Courses Available`
	} else {
		courseData = result.map((course) => {
			console.log(course);

		if(adminUser === false || !adminUser){
		cardFooter =
		`
			<a href="./singleCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block selectButton">
				Select Course
			</a>
		`
		} else {
			
			if(course.isActive == true){
				cardFooter = 
				`
					<a href="./singleCourse.html?courseId=${course._id}" class="btn btn-primary text-white btn-block editButton">
						Edit Course
					</a>
				
				
					<a href="./singleCourse.html?courseId=${course._id}" class="btn btn-danger text-white btn-block archiveButton">
						Archive Course
					</a>
				`
			} else {
				cardFooter =
				`
					<a href="./singleCourse.html?courseId=${course._id}" class="btn btn-success text-white btn-block unarchiveButton">
						Unarchive Course
					</a>
				`
			}
		}

			return(

			`
			<div class="col-md-6 my-5">
				<div class="card">
					<div class="card-body">
						<h5 class="card-title">
							${course.name}
						</h5>
						<p class="card-text text-left"> 
							${course.description}
						</p>
						<p class="card-text text-right">
							${course.price}
						</p>
					</div>
					<div class="card-footer">
							${cardFooter}

					</div>
				</div>
			</div>

			`

			)
		}).join('');
	}

	let container = document.querySelector(`#courseContainer`)
	container.innerHTML = courseData
})