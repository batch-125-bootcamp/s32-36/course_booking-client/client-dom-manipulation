console.log(window.location.search)
let params = new URLSearchParams(window.location.search);

// method of URLSearchParams
	// get()
let courseId = params.get('courseId');
console.log(courseId)

// target the elements

let courseName = document.querySelector('#courseName');
let courseDesc = document.querySelector('#courseDesc');
let coursePrice = document.querySelector('#coursePrice');
let enrollmentContainer = document.querySelector('#enrollmentContainer')

let token = localStorage.getItem('token');

fetch(`http://localhost:3000/api/courses/${courseId}`, 
		{
			method: "GET",
			headers: {
				"Authorization":`Bearer ${token}`
		}
	}
)
.then(result => result.json())
.then(result => {

	console.log(result)
	console.log(result.name)

	courseName.innerHTML = result.name
});