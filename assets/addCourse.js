let createCourse = document.querySelector('#createCourse')

createCourse.addEventListener("submit", (e) => {
	e.preventDefault()

	let courseName = document.querySelector('#courseName').value
	let coursePrice = document.querySelector('#coursePrice').value
	let courseDesc = document.querySelector('#courseDesc').value

	if(courseName !== "" && coursePrice !== "" && courseDesc !== ""){
		
		let token = localStorage.getItem("token"); 

		fetch("http://localhost:3000/api/courses/addCourse", 
			{
				method: "POST",
				headers: {
					"Content-Type" : "application/json",
					"Authorization" : `Bearer ${token}` 
				},
				body: JSON.stringify({
					name: courseName,
					price: coursePrice,
					description: courseDesc
				})

			}
		)
		.then(result => result.json())
		.then(result => {
			if(result === true){
				alert(`Course successfully created!`)

				window.location.replace("./courses.html")
			} else {
				alert(`Course creation failed, something went wrong!`)
			}
		})
	}
})